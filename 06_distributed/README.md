# Distributed Learning

基于MindSpore开源深度学习框架的实验指导，仅用于教学或培训目的。

部分内容来源于开源社区、网络或第三方。如果有内容侵犯了您的权力，请通过issue留言，或者提交pull request。

请前往[MindSpore开源社区](https://www.mindspore.cn/)获取更多视频和文档教程。

## 内容

1. [性能优化—图算融合](graph_kernel)
2. [性能优化—混合精度](mixed_precision)

# Pratical Course of MindSpore

基于MindSpore和华为云平台在NLP、CV、Audio、GAN等领域的实践案例精品课程，用于帮助使用者在线学习MindSpore。

部分内容来源于开源社区、网络或第三方。如果有内容侵犯了您的权力，请通过issue留言，或者提交pull request。

请前往[MindSpore开源社区](https://www.mindspore.cn/)获取更多视频和文档教程。

## 内容

1. [MindSpore入门](beginner_of_MS)
3. [手写数字识别[LeNet5][Ascend/CPU/GPU]](lenet5)
4. [模型保存和加载[LeNet5][Ascend/CPU/GPU]](https://mindspore.cn/tutorials/zh-CN/r1.7/beginner/save_load.html)
5. [优化器对比[Dense]](optimizer)
6. [正则化对比[Conv1x1]](regularization)
7. [端侧图像分类应用部署](https://mindspore.cn/tutorials/zh-CN/r1.7/beginner/infer.html)
8. [端侧C++推理流程](lite_cpp_inference)

# Application Example

基于MindSpore 1.7+ 全场景AI框架的实验样例指导，仅用于教学或培训目的。

部分内容来源于开源社区、网络或第三方。如果有内容侵犯了您的权力，请通过issue留言，或者提交pull request。

请前往[MindSpore开源社区官网](https://mindspore.cn/tutorials/zh-CN/master/index.html)获取更多视频和文档教程。

## 内容

建议先学习[MindSpore初学教程](https://mindspore.cn/tutorials/zh-CN/master/index.html)，了解MindSpore及其初步用法。

建议再学习[MindSpore进阶教程](https://mindspore.cn/tutorials/zh-CN/master/advanced/linear_fitting.html)，了解如何自定义MindSpore的模块以及使用方式。

然后开始了解如何通过ModelArts训练作业Jobs、ModelArts Notebook、或本地环境进行实验，以及三者的注意事项。

### 计算机视觉的艺术GAN

1. [GAN-为什么要对抗生成而不是和谐](GAN)
2. [x] [DCGAN-GAN网络遇到卷积](DCGAN)
3. [Pix2Pix-图像翻译来啦](Pix2Pix)
4. [SRGAN-图像超分播放](SRGAN)
5. [WGAN-GAN网络训练更容易](WGAN)
6. [CycleGAN和StyleGAN- GAN遇到艺术](CycleGAN-StyleGAN)

## 版权

- [Apache License 2.0](LICENSE)

## 引用

如果您觉得这个项目对您有帮助，请您考虑给MindSpore Course仓关注/点赞，另外可以引用：

```latex
@misc{MindSpore Course 2022,
    title={{MindSpore Course}:MindSpore Course Education and Examples},
    author={MindSpore Course Contributors},
    howpublished = {\url{https://gitee.com/mindspore/course}},
    year={2021}
}
```
# Application Example

基于MindSpore 1.7+ 全场景AI框架的实验样例指导，仅用于教学或培训目的。

部分内容来源于开源社区、网络或第三方。如果有内容侵犯了您的权力，请通过issue留言，或者提交pull request。

请前往[MindSpore开源社区官网](https://mindspore.cn/tutorials/zh-CN/master/index.html)获取更多视频和文档教程。

## 内容

建议先学习[MindSpore初学教程](https://mindspore.cn/tutorials/zh-CN/master/index.html)，了解MindSpore及其初步用法。

建议再学习[MindSpore进阶教程](https://mindspore.cn/tutorials/zh-CN/master/advanced/linear_fitting.html)，了解如何自定义MindSpore的模块以及使用方式。

然后开始了解如何通过ModelArts训练作业Jobs、ModelArts Notebook、或本地环境进行实验，以及三者的注意事项。

### 计算机视觉CNN

1. [AlexNet-深度学习热潮的奠基作](Alexnet)
2. [VGG-使用3x3卷积构造更深的网络](VGG)
3. [GoogleNet-使用并行架构构造更深的网络](GoogleNet)
4. [x] [ResNet-构建深层网络都需要有的残差连接](ResNet)
5. [x] [MobileNet-适合终端设备的小CNN](MobileNet)
6. [GhostNet-诺亚创新高精度小模型](GhostNet)
7. [x] [EfficientNet-通过架构搜索得到的CNN](EfficientNet)
8. [MoCo-无监督训练来玩视觉](MoCo)

## 版权

- [Apache License 2.0](LICENSE)

## 引用

如果您觉得这个项目对您有帮助，请您考虑给MindSpore Course仓关注/点赞，另外可以引用：

```latex
@misc{MindSpore Course 2022,
    title={{MindSpore Course}:MindSpore Course Education and Examples},
    author={MindSpore Course Contributors},
    howpublished = {\url{https://gitee.com/mindspore/course}},
    year={2021}
}
```
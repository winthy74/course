# Application Example

基于MindSpore 1.7+ 全场景AI框架的实验样例指导，仅用于教学或培训目的。

部分内容来源于开源社区、网络或第三方。如果有内容侵犯了您的权力，请通过issue留言，或者提交pull request。

请前往[MindSpore开源社区官网](https://mindspore.cn/tutorials/zh-CN/master/index.html)获取更多视频和文档教程。

## 内容

建议先学习[MindSpore初学教程](https://mindspore.cn/tutorials/zh-CN/master/index.html)，了解MindSpore及其初步用法。

建议再学习[MindSpore进阶教程](https://mindspore.cn/tutorials/zh-CN/master/advanced/linear_fitting.html)，了解如何自定义MindSpore的模块以及使用方式。

然后开始了解如何通过ModelArts训练作业Jobs、ModelArts Notebook、或本地环境进行实验，以及三者的注意事项。

### 通用技术介绍

1. [SGD到ADAM-最常用的优化器](Normal/SGD)
2. [Momentum-真的有效](Normal/momentum)

### 计算机视觉CNN

1. [AlexNet-深度学习热潮的奠基作](CNN/Alexnet)
2. [VGG-使用3x3卷积构造更深的网络](CNN/VGG)
3. [GoogleNet-使用并行架构构造更深的网络](CNN/GoogleNet)
4. [x] [ResNet-构建深层网络都需要有的残差连接](CNN/ResNet)
5. [x] [MobileNet-适合终端设备的小CNN](CNN/MobileNet)
6. [GhostNet-诺亚创新高精度小模型](CNN/GhostNet)
7. [x] [EfficientNet-通过架构搜索得到的CNN](CNN/EfficientNet)
8. [MoCo-无监督训练来玩视觉](CNN/MoCo)

### 计算机视觉Transformer

1. [x] [ViT-当Transformer进入CV领域](Transformer/ViT)
2. [Swin Transformer-多层视觉Transformer](Transformer/Swin-T)
3. [MLP-Mixer-全连接层替换注意力机制](Transformer/MLP-Mixer)
4. [MAE-视觉通向大模型](Transformer/MAE)

### 计算机视觉的艺术GAN

1. [GAN-为什么要对抗生成而不是和谐](GAN/GAN)
2. [x] [DCGAN-GAN网络遇到卷积](GAN/DCGAN)
3. [Pix2Pix-图像翻译来啦](GAN/Pix2Pix)
4. [SRGAN-图像超分播放](GAN/SRGAN)
5. [WGAN-GAN网络训练更容易](GAN/WGAN)
6. [CycleGAN-GAN遇到艺术](GAN/CycleGAN)
7. [StyleGAN-GAN再次遇到艺术](GAN/StyleGAN)

### 计算视觉检测分割Object Detection

1. [从R-CNN到Faster R-CNN的发展过程](Detectin/Faster-R-CNN)
2. [YOLO-看一眼就检测出来了](Detectin/YOLO)
2. [SSD-特征金字塔](Detectin/SSD)
3. [FCN-第一次深度分割](Detectin/FCN)
3. [Mask R-CNN-检测更多的想象力](Detectin/Mask-RCNN)
5. [CentorNet-检测不需要Anchor啦](Detectin/CentorNet)
6. [DETR-检测中遇到Transformer](Detectin/DETR)

### 自然语言处理-NLP

1. [RNN-初见长时间序列](NLP/RNN)
2. [LSTM-防止爆炸的长时间序列](NLP/LSTM)
3. [Seq2Seq-与Encoder的碰撞](NLP/Seq2Seq)
4. [Attention-注意力加持](NLP/Attention)
5. [Transformer-DL的第四大类架构](NLP/Transformer)
6. [BERT-走向预训练大模型基础](NLP/BERT)

### 语音信号处理-Audio

1. [DeepSpeech-语音ASR有点意思](Audio/DeepSpeech)

## 版权

- [Apache License 2.0](LICENSE)

## 引用

如果您觉得这个项目对您有帮助，请您考虑给MindSpore Course仓关注/点赞，另外可以引用：

```latex
@misc{MindSpore Course 2022,
    title={{MindSpore Course}:MindSpore Course Education and Examples},
    author={MindSpore Course Contributors},
    howpublished = {\url{https://gitee.com/mindspore/course}},
    year={2021}
}
```
# course

基于MindSpore开源深度学习框架的实验指导，仅用于教学或培训目的。

部分内容来源于开源社区、网络或第三方。如果有内容侵犯了您的权力，请通过issue留言，或者提交pull request。

请前往[MindSpore开源社区](https://www.mindspore.cn/)获取更多视频和文档教程。

## 内容

建议先学习[MindSpore初学教程](https://mindspore.cn/tutorials/zh-CN/master/index.html)，了解MindSpore及其初步用法。

建议再学习[MindSpore进阶教程](https://mindspore.cn/tutorials/zh-CN/master/advanced/linear_fitting.html)，了解如何自定义MindSpore的模块以及使用方式。

然后开始了解如何通过ModelArts训练作业Jobs、ModelArts Notebook、或本地环境进行实验，以及三者的注意事项。

### 精品课程

MindSpore与顶尖高校联合打造面向AI领域的精品课程，用于满足高校教学和人才培养的需求。

#### [机器学习原理](01_ML)

#### [计算机视觉](02_CV)

#### [自然语言处理](03_NLP)

#### [语音信号处理](04_SSP)

#### [AI系统原理](05_AI)

#### [分布式学习](06_distributed)

#### [华为云](07_cloud_base)


### 应用案例

#### [培训案例](training_example)

#### [应用案例](application_example)


## 版权

- [Apache License 2.0](LICENSE)
- [Creative Commons License version 4.0](LICENSE-CC-BY-4.0)

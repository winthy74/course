<!--  Thanks for sending a pull request!  -->

**What type of PR is(select one)**

> /kind bug
> /kind task
> /kind feature

**What does this PR do / why do we need it**:

*give a description of these bug pull request.*

**Which issue(s) this PR fixes**:

Fixes #ISSUES

**CheckList**:

- [] I have added correct copyrights for every code file.
- [] I have removed all the redundant code and comments.
- [] I have made sure that I won't expose any personaly information such as local path with user name, local IP, etc.
- [] I have commented my code, particularly in hard-to-understand areas. All the comments in code files are in English.
- [] I have squashed all the commits into one.
- [] If you are contributing a new example, please check:
    - [] I have added at least one `README.md`.
    - [] The example can run on `ipython notebook`.
